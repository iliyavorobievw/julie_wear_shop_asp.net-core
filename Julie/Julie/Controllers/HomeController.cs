﻿using Julie.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Julie.Controllers
{
    public class HomeController : Controller
    {
       
        public IActionResult Product()
        {
            return View();
        }
        public IActionResult Index()
        {
            var path = @"C:\Users\qwe\Desktop\Julie\Julie\wwwroot\Files\Products.txt";
            using (FileStream fstream = new FileStream(path, FileMode.Open))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.ReadAsync(buffer, 0, buffer.Length);

                string textFromFile = Encoding.Default.GetString(buffer);

                var b = textFromFile.Split('|');
                int countProducts = b.Length / 4;

                var products = new List<Product>();

                int a1 = 0; int a2 = 1; int a3 = 2; int a4 = 3;

                for (int i = 0; i < countProducts; i++)
                {
                    var product = new Product
                    {
                        name = b[a1].Trim(),
                        inStock = b[a2].Trim(),
                        price = b[a3].Trim(),
                        filepath = b[a4].Trim()

                    };
                    a1 += 4; a2 += 4; a3 += 4; a4 += 4;
                    products.Add(product);
                }
                return View(products);
            }
        }
    }
}
