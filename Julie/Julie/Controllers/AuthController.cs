﻿using Julie.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Julie.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Auth()
        {
            return View();
        }
       
        public IActionResult Login(LoginViewModel model)
        {
            var path = @"C:\Users\qwe\Desktop\Julie\Julie\wwwroot\Files\Users.txt";
            using (FileStream fstream = new FileStream(path, FileMode.Open))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.ReadAsync(buffer, 0, buffer.Length);

                string textFromFile = Encoding.Default.GetString(buffer);

                var b = textFromFile.Split('|');
                int countUser = b.Length / 4;

                var users = new List<User>();

                int a1 = 0; int a2 = 1; int a3 = 2; int a4 = 3;

                for (int i = 0; i < countUser; i++)
                {
                    var us = new User
                    {
                        name = b[a1].Trim(),
                        email = b[a2].Trim(),
                        password = b[a3].Trim(),
                        role = b[a4].Trim()

                    };
                    a1 += 4; a2 += 4; a3 += 4; a4 += 4;
                    users.Add(us);
                }
                User user = users.FirstOrDefault(u => u.email == model.Email && u.password == model.Password);

                if (user != null)
                {
                    if (user.role == "1")
                    {
                        return RedirectToAction("Admin", "Admin");
                    }

                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }

            }
            return View(model);
        }

    }
}

