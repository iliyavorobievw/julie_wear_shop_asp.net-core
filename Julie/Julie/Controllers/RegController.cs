﻿using Julie.Data.Models;
using Julie.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Julie.Controllers
{
    public class RegController : Controller
    {
        [HttpGet]
        public IActionResult Reg()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel model)
        {
            User user = new User { name = model.Name + ' ' + model.Surname, email = model.Email, password = model.Password, role = "0" };

            string userWrite = $"{user.name}|{user.email}|{user.password}|{user.role}|";
            string path = @"C:\Users\qwe\Desktop\Julie\Julie\wwwroot\Files\Users.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(userWrite);
            }
            return RedirectToAction("Index", "Home");

        }
    }


}
