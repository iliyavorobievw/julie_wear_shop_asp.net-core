﻿using Julie.Data.Models;
using Julie.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Julie.Controllers
{
    public class AdminController : Controller
    {

        [HttpGet]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult AddProduct(AddproductViewModel model)
        {
            Product product = new Product
            {
                name = model.Name,
                price = model.Price,
                filepath = model.Filepath,
                inStock = model.Instock
            };

            string productWrite = $"{product.name}|{product.inStock}|{product.price}|{product.filepath}|";
            string path = @"C:\Users\qwe\Desktop\Julie\Julie\wwwroot\Files\Products.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(productWrite);
            }
            return RedirectToAction("Admin", "Admin");

        }
    }
}
