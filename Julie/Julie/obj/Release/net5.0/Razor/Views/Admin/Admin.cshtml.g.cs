#pragma checksum "C:\Users\qwe\Desktop\Julie\Julie\Views\Admin\Admin.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "569f083f7c5e3bd4b0d34fcbb5e3ab0a9eec2a7c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Admin), @"mvc.1.0.view", @"/Views/Admin/Admin.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"569f083f7c5e3bd4b0d34fcbb5e3ab0a9eec2a7c", @"/Views/Admin/Admin.cshtml")]
    public class Views_Admin_Admin : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<!DOCTYPE html>
<html lang=""en"">
<head>
    <meta charset=""UTF-8"">
    <link rel=""shortcut icon"" href=""~/admin_styles/icons/mini-logo.ico"" type=""image/x-icon"">

    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Admin</title>
    <link rel=""stylesheet"" href=""~/admin_styles/style.css"">

</head>
<body>
    <div class=""sidebar"">
        <div class=""sidebar-brand"">
            <h1><img src=""~/admin_styles/icons/mini-logo.png"" width=""30px"" height=""30px""");
            BeginWriteAttribute("alt", " alt=\"", 690, "\"", 696, 0);
            EndWriteAttribute();
            WriteLiteral(">Julie</h1>\r\n        </div>\r\n        <div class=\"sidebar-menu\">\r\n            <ul>\r\n                <li>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 824, "\"", 831, 0);
            EndWriteAttribute();
            WriteLiteral(" class=\"active\">\r\n                        <img src=\"~/admin_styles/icons/sales.png\" width=\"30px\" height=\"30px\"");
            BeginWriteAttribute("alt", " alt=\"", 942, "\"", 948, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n\r\n                        <span>Sales</span>\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 1091, "\"", 1098, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <img src=\"~/admin_styles/icons/human.png\" width=\"30px\" height=\"30px\"");
            BeginWriteAttribute("alt", " alt=\"", 1194, "\"", 1200, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <span>Users</span>\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 1341, "\"", 1348, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <img src=\"~/admin_styles/icons/wear.png\" width=\"30px\" height=\"30px\"");
            BeginWriteAttribute("alt", " alt=\"", 1443, "\"", 1449, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <span>Products</span>\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"main-content\">\r\n        <header>\r\n            <h1>\r\n                <label");
            BeginWriteAttribute("for", " for=\"", 1688, "\"", 1694, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                    <span class=""las la-bars""></span>
                </label>
                Admin panel Julie
            </h1>
            <div class=""user-wrapper"">
                <img src=""~/admin_styles/icons/human.png"" width=""30px"" height=""30px""");
            BeginWriteAttribute("alt", " alt=\"", 1957, "\"", 1963, 0);
            EndWriteAttribute();
            WriteLiteral(@">
                <div>
                    <h4>Admin name</h4>
                    <small>role Admin</small>
                </div>
            </div>
        </header>

        <main>
            <div class=""cards"">
                <div class=""card-single"">
                    <div>
                        <h1>Add sales</h1>
                        <table>
                            <tr>
                                <td> <span>Text sale</span> </td>
                                <td> <span><input type=""text""></span> </td>
                            </tr>
                            <tr>
                                <td> <span>Amount sale</span> </td>
                                <td> <span><input type=""number""></span> </td>
                            </tr>

                        </table>
                        <a");
            BeginWriteAttribute("href", " href=\"", 2831, "\"", 2838, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""shine-button"">Add sale</a>

                    </div>
                    <div>
                        <span class=""las la-users""></span>
                    </div>
                </div>
                <div class=""card-single"">
                    <div>
                        <h1>Add User</h1>
                        <table>
                            <tr>
                                <td>Name</td>
                                <td><input type=""text""></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><input type=""email""></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td><input type=""password""></td>
                            </tr>
                        </table>
                        <a");
            BeginWriteAttribute("href", " href=\"", 3794, "\"", 3801, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""shine-button"">Add User</a>

                    </div>
                    <div>
                        <span class=""las la-users""></span>
                    </div>
                </div>
                <div class=""card-single"">
                    <div>
                        <h1>Add Product</h1>
                        <table>
                            <tr>
                                <td><span>Name</span></td>
                                <td><input type=""text""></td>
                            </tr>

                            <tr>
                                <td><span>Price</span></td>
                                <td><span><input type=""number""></span> </td>
                            </tr>

                            <tr>
                                <td><span>Count</span></td>
                                <td><span><input type=""number""></span> </td>
                            </tr>

                            <tr>
                       ");
            WriteLiteral("         <td><span>Size</span></td>\r\n                                <td><span><input type=\"text\"></span> </td>\r\n                            </tr>\r\n                        </table>\r\n                        <a");
            BeginWriteAttribute("href", " href=\"", 5034, "\"", 5041, 0);
            EndWriteAttribute();
            WriteLiteral(@" class=""shine-button"">Add Product</a>

                    </div>
                    <div>
                        <span class=""las la-users""></span>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
