﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Julie.Data.Models
{
    public class Sales
    {
        public int id { get; set; }
        public string textSale { get; set; }
        public int amount { get; set; }
    }
}
