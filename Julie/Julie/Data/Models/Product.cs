﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Julie.Data.Models
{
    public class Product
    {
        public string name { get; set; }
        public string inStock { get; set; }
        public string price { get; set; }
        public string filepath { get; set; }
    }
}
