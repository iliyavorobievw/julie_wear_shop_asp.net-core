﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Julie.Data.Models
{
    public class Feedback
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}
